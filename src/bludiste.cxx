/*
 *      bludiste.cxx
 *
 *      Copyright 01.11.2011 00:41:09 CEST Jan Dluhos  DLU0023 <jan.dluhos.st@vsb.cz> || <bougi23@seznam.cz>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */

// POPIS: hra Bludiste

#include <iostream> // pro debugovani (cout,endl atp.)
#include <ctime> // knihovna pro casove funkce
#include <fstream> // knihovna pro souborove proudy
#include <allegro.h> // Knihovna pro hern� funkce, vykreslovani grafiky, bmp souboru apod.
#include "bludiste.h" // globalni veci, promenne, prototypy a dalsi
#include "urovne.h" // souradnice objektu a cilu

using namespace std;

int main(int argc, char *argv[])
{
	inicializace(argc,argv);

	// Hlavni smycka hry, ktera se ukonci stiskem klavesy ESC
	while(!konec)
	{                 
		vykresliBuffer(); // pred kazdym vykreslovanim tzv. dopredne vykreslovani
		// aktualizace souradnic pacmana 
		o1x = pl; // souradnice leveho horniho bodu
		o1y = nd+20; // to same
		o2x = pl+20; // souradnice praveho dolniho bodu
		o2y = nd+40; // to same
				
		// nacteni BLUDISTE podle aktualniho cisla levelu
		generujMapu();
		// nacteni CILE podle aktualniho cisla levelu
		generujCile();
		// OVLADANI a detekce KOLIZI a otaceni bitmapy pacmana podle smeru
		ovladani();

		// Pokud v prubehu hry se stiskene klavesa ESC, zeptam se jestli chces opravdu skoncit !
		if(key[KEY_ESC])
		{
			clear_to_color(screen, makecol(0,0,0));
			textprintf_ex(screen, font, SIRKA_OKNA/6, VYSKA_OKNA/2 , makecol(255, 100, 200), -1, "Opravdu chcete opustit hru ? OPUSTIT hru <ENTER> nebo ZPET do hry <ESC> .");
			readkey();
			if(key[KEY_ENTER])
				konec = true;
			else
				konec = false;
		}
		// Vypsani nejrychlejsiho casu dole 
		if(zjistiNejCas() == 0)
			textprintf_ex(buffer, font, 10, VYSKA_OKNA-10, makecol(255, 100, 200), -1, "Nejrychlejsi cas teto urovne jeste nebyl vytvoren !! ");		
		else	
			textprintf_ex(buffer, font, 10, VYSKA_OKNA-10, makecol(255, 100, 200), -1, "Nejrychlejsi cas teto urovne je: %i sekund.", zjistiNejCas());
	// Ukonceni hry po projeti celeho bludiste
		if(level == 4)
			return 0;
    }

	// KONCIME takze finito a cistime !
	clear(buffer);
	clear(screen);
	allegro_exit();
	
	return 0;
}
END_OF_MAIN();

/// FUNKCE INICIALIZECE
/// @param argc Podobne jako v main ma za ukol spocitat kolik bylo vlozeno parametru
/// @param argv Ma za ukol zapsany paramatr ulozit do pole tak aby se z nej v teto funkci dalo cist
/// Funkce inicializuje potrebne prvky z knihovni funkce allegro.h a take inicilizuje hraci plochu, buffer, casovani
/// Funkci jsou predany vstupni argumenty z prikazove radky
/// Napriklad parametr -d spusti bludiste v DEBUG modu anebo parametr -f spusti bludiste ve fullscreen modu
void inicializace(int argc, char *argv[])
{
 	allegro_init(); // init allegra
 	install_keyboard(); // init klavesnice
   	initBuffer(); // init bufferu
   	
	//////////////////////////
	// CASOVANI
	LOCK_VARIABLE(ticks);
    LOCK_FUNCTION(ticker);
	install_int(ticker,10);
	install_int_ex(ticker, BPS_TO_TIMER(1));

	//////////////////////////
	// PARAMETRY z PRIKAZOVE RADKY (fullscreen, windowscreen,debug)
    for (int i = 0; i < argc; i++) 
    {
		// prvni "parametr" je pomlcka
		if (argv[i][0] == '-') 
		{
			// Dalsi parametry a udelam co se po vyzaduje
			if(argv[i][1] == 'f')
				set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, SIRKA_OKNA,VYSKA_OKNA,0,0); // fullscreen
			else if (argv[i][1] == 'd')
				DEBUG = 1;
			else if (argv[i][1] == 'h')
			{	
				cout << "Parametry: " << endl;
				cout << "------------" << endl;
				cout << "-f parametr pro spusteni bludiste ve fullscreen modu" << endl;
				cout << "-d parametr pro spusteni bludiste s debugovacim vystupem na konzoli" << endl;
				cout << "-h parametr pro tuto napovedu" << endl;
				exit(0);
			}
		}
		else
		{
			set_gfx_mode(GFX_AUTODETECT_WINDOWED, SIRKA_OKNA,VYSKA_OKNA,0,0); // okno
			set_window_title("Bludiste (1.0), Naprogramoval Jan Dluhos, DLU0023");
		}
	}

	//
	if(DEBUG)
		cout << "DEBUG MOD ZAPNUT: " << endl;
	//
	// CAS zacatek mereni
	tstart = time(0);
   	initBludiste();
}


/// FUNKCE OVLADANI
/// Funkce pro ovladani objektu (bludistaka) po herni mape, pokud nedojde ke kolizi bludistaka s pevnym objektem tak jde bludistak danym smerem o krok
/// Funkce cte zjistuje jaka klavesa byla stisknuta (sipka v pravo,levo,nahoru nebo dolu) a podle toho zmeni bitmapu bludista a udela krok patricnym smerem
/// Funkce uzce spolupracuje s funkci kolize() 
inline void ovladani()
{
	// BITMAPY pacmana (naHoru,Dolu,doLeva,doPrava)
	bl_h = load_bitmap("./imgs/bl_h_20.bmp", 0);
	bl_d = load_bitmap("./imgs/bl_d_20.bmp", 0);
	bl_l = load_bitmap("./imgs/bl_l_20.bmp", 0);
	bl_p = load_bitmap("./imgs/bl_p_20.bmp", 0);

	//
	readkey();
	if(key[KEY_RIGHT])
	{
		initBludiste();
		//
		for(int ip=0;ip<=sourObjMAX;ip++)
		{ // je-li DETEKOVANA kolize   |	o1x+1					|			o1y				 |			o2x				  |			o2y
			if(kolize(o1x+krok,o1y,o2x,o2y,souradniceObj[level][ip][0],souradniceObj[level][ip][1],souradniceObj[level][ip][2],souradniceObj[level][ip][3])) //detekuje kontakt dopredu
			{
				pl=souradniceObj[level][ip][0]-21-krok; //  -23 // jeden pixely od statickeho 				
				if(DEBUG)
					cout << "(KOLIZE)(DOPRAVA) souradnice kolize je :" << 	souradniceObj[level][ip][0]-3 << endl; //ok
			}
		}
		// neni-li DETEKOVANA kolize jdu dal o krok
    	masked_blit(bl_p, buffer, 0,0,o1x,o1y,20,20);
        pl+=krok;
		destroy_bitmap(bl_p);
	}
//
	else if(key[KEY_LEFT])
	{
		initBludiste();
		//
		for(int ip=0;ip<=sourObjMAX;ip++)
		{ 
			if(kolize(o1x,o1y,o2x-krok,o2y,souradniceObj[level][ip][0],souradniceObj[level][ip][1],souradniceObj[level][ip][2],souradniceObj[level][ip][3]))
			{
				pl=souradniceObj[level][ip][2] +1+krok; // +3
				if(DEBUG)
					cout << "(KOLIZE)(DOLEVA) souradnice kolize je :" << 	souradniceObj[level][ip][2] +3 << endl; //ok
			}
		}
		masked_blit(bl_l, buffer, 0,0,o1x,o1y,20,20);
		pl-=krok;
		destroy_bitmap(bl_l);
	}
//
	else if(key[KEY_DOWN])
	{
		initBludiste();
		//
		for(int ip=0;ip<=sourObjMAX;ip++)
		{ 
			if(kolize(o1x,o1y+krok,o2x,o2y,souradniceObj[level][ip][0],souradniceObj[level][ip][1],souradniceObj[level][ip][2],souradniceObj[level][ip][3]))			
			{
				nd=souradniceObj[level][ip][1] -41-krok; // -43					
				if(DEBUG)
					cout << "(KOLIZE)(DOLU) souradnice kolize je :" << 	souradniceObj[level][ip][1] -3 << endl;
			}
		}
		masked_blit(bl_d, buffer, 0,0,o1x,o1y,20,20);		
		nd+=krok;
		destroy_bitmap(bl_d);
	}
//
	else if(key[KEY_UP])
	{
		initBludiste();
		//
		for(int ip=0;ip<=25;ip++)
		{
			if(kolize(o1x,o1y,o2x,o2y-krok,souradniceObj[level][ip][0],souradniceObj[level][ip][1],souradniceObj[level][ip][2],souradniceObj[level][ip][3]))			
			{
				nd=souradniceObj[level][ip][3] -19+krok; // -17				
				if(DEBUG)
					cout << "(KOLIZE)(NAHORU) souradnice kolize je :" << 	souradniceObj[level][ip][3] +3 << endl;	
			}
		}
		masked_blit(bl_h, buffer, 0,0,o1x,o1y,20,20);
		nd-=krok;
		destroy_bitmap(bl_h);
	}


// staticke zamezeni prochazeni zdmi okna
	if(pl < 0) pl=0;
	if(nd < 0)	nd=0;
	if(pl > (SIRKA_OKNA-20)) pl=SIRKA_OKNA-20;
	if(nd > (VYSKA_OKNA-80)) nd=VYSKA_OKNA-80;
}


/// FUNKCE KOLIZE
/// @param o1x Levy horni roh dynamickeho objektu (bludistaka)
/// @param o1y Levy spodni roh dynamickeho objektu  (bludistaka) 
/// @param o1w Pravy horni roh dynamickeho objektu (bludistaka)
/// @param o1h Pravy spodni roh dynamickeho objektu (bludistaka)
/// @param o2x Levy horni roh statickeho objektu (zdi)
/// @param o2y Levy spodni roh statickeho objektu  (zdi) 
/// @param o2w Pravy horni roh statickeho objektu (zdi)
/// @param o2h Pravy spodni roh statickeho objektu (zdi)
/// @return Vraci 1 je-li kolize, v opacnem pripade vraci 0
/// Funkce pro detekci kolize mezi dynamickym a statickym objektem
/// Tato funkce ma 8 vstupnich parametru, prvni ctyri jsou souradnice ctyr rohu bludistaka a dalsi ctyri jsou souradni rohu statickeho objektu
inline bool kolize(UINT o1x, UINT o1y, UINT o1w, UINT o1h, UINT o2x, UINT o2y, UINT o2w, UINT o2h) //o1 dynamicky objekt, o2 staticky objekt
{
	if(o1y<o2y) // pohyblivy je nad statickym
	{
		if(o1y<(o2y-20) || o1x<(o2x-20) || (o1w-20)>o2w ||  (o1h-20)>o2h )// neni kolize
			return 0;
		else // je kolize
			return 1;
	}
	if(o1x<o2x) // pohyblivy je v levo od statickeho
	{
		if(o1y<(o2y-20) || o1x<(o2x-20) || (o1w-20)>o2w ||  (o1h-20)>o2h )// neni kolize
			return 0;
		else // je kolize
			return 1;
	}
	if(o1w>o2w) // pohyblivy je v pravo od statickeho
	{
		if(o1y<(o2y-20) || o1x<(o2x-20) || (o1w-20)>o2w ||  (o1h-20)>o2h )// neni kolize		
			return 0;
		else // je kolize
			return 1;
	}
	if(o1h>o2h) // pohyblivy je pod statickym
	{
		if(o1y<(o2y-20) || o1x<(o2x-20) || (o1w-20)>o2w ||  (o1h-20)>o2h )// neni kolize
			return 0;
		else // je kolize
			return 1;
	}
	return -1;
}


/// FUNKCE GENERUJMAPU
/// Funkce pro vykresleni statickych objektu (zdi) ze zadanych souradnic z pole souradniceObj[][][], pokud je zaply debug mod (DEBUG 1) tak se vykresli bez grafiky 
inline void generujMapu()
{
	if(DEBUG)
	{
		// defaultni DEBUGOVACI vykreslovani objektu na testovani kolizi !_NEMAZAT_
		for(int i=0;i<=sourObjMAX;i++)	
		{
       			rectfill(buffer, souradniceObj[level][i][0], souradniceObj[level][i][1], souradniceObj[level][i][2], souradniceObj[level][i][3], makecol(123,123,123));
		}
	}
	else
	{
		// TODO: zpusobuje zvetsovani sse v pameti !!!!!
		BITMAP *zed;
		zed = load_bitmap("./imgs/zed.bmp", 0);
		
		//////////////////////////
		// Vykreslovani grafiky zdi , do vysky nebo do sirky
		// TODO: posledni pixel vykreslit na bilo napad by Josef

		// TODO: Ukazatele, ted kdyz uz to umim
		// vytvoreni pomocnych
		int v = 0; // vyska
		int vz = 0; // vyska zbytek po deleni
		int s = 0; // sirka
		int sz = 0; // sirka zbytek po deleni

		    for (int i=0;i<sourObjMAX;i++) // tolikrat kolikrat je objektu tedy sourObjMAX
		    {
				//SIRSI ZED
		    	if ( ((souradniceObj[level][i][3] - souradniceObj[level][i][1])/20) > ((souradniceObj[level][i][2] - souradniceObj[level][i][0])/20) ) //je-li vyssi zed
		     	{
		      		if( ((souradniceObj[level][i][3] - souradniceObj[level][i][1])%20) == 0) // neni-li zbytek vykresluju normalne
		        	{
					v= (souradniceObj[level][i][3] - souradniceObj[level][i][1])/20; // vyska
			        	// VYKRESLUJEME ZED
				       	for(int k=0;k<=v;k++)
					{
                                		if(k!=v)
                                		blit(zed, buffer, 0,0,souradniceObj[level][i][0],souradniceObj[level][i][1]+(k*20),20,20);
                        		}
				}
				
				else // je-li zbytek
				{
					v= (souradniceObj[level][i][3] - souradniceObj[level][i][1])/20; // vyska
					vz= (souradniceObj[level][i][3] - souradniceObj[level][i][1])%20; // zbytek
					// VYKRESLUJEME ZED
					for(int k=0;k<=v;k++)
					{
						if(k==v) // jeli ctverecek posledni dopoctu jeho velikost a vyplnim
							// TODO: TADY POZOR MUSIM UDELAT AT POSLEDNI PIXEL JE NABILO AT TO VYPADA !!
							blit(zed, buffer, 0,0,souradniceObj[level][i][0],souradniceObj[level][i][1]+(k*20),20,vz);
				
						else // jinac vykreslujem normalne
							blit(zed, buffer, 0,0,souradniceObj[level][i][0],souradniceObj[level][i][1]+(k*20),20,20);
                          		}
					}
				}
				//SIRSI ZED
				if ( ((souradniceObj[level][i][3] - souradniceObj[level][i][1])/20) < ((souradniceObj[level][i][2] - souradniceObj[level][i][0])/20) ) //je-li sirsi zed
				{
					if( ((souradniceObj[level][i][2] - souradniceObj[level][i][0])%20) == 0)
					{
						s= (souradniceObj[level][i][2] - souradniceObj[level][i][0])/20; // sirka
						// VYKRESLUJEME ZED
						for(int k=0;k<=s;k++)
						{
							if(k!=s)
							blit(zed, buffer, 0,0,souradniceObj[level][i][0]+(k*20),souradniceObj[level][i][1],20,20);
						}
					}
					else
					{
						s= (souradniceObj[level][i][2] - souradniceObj[level][i][0])/20; // sirka
						sz= (souradniceObj[level][i][2] - souradniceObj[level][i][0])%20; // zbytek
						// VYKRESLUJEME ZED
						for(int k=0;k<=s;k++)
						{
							if(k==s) // jeli ctverecek posledni dopoctu jeho velikost a vyplnim
								// TODO: TADY POZOR MUSIM UDELAT AT POSLEDNI PIXEL JE NABILO AT TO VYPADA !!					
								blit(zed, buffer, 0,0,souradniceObj[level][i][0]+(k*20),souradniceObj[level][i][1],sz,20);
							else // jinac vykreslujem normalne
								blit(zed, buffer, 0,0,souradniceObj[level][i][0]+(k*20),souradniceObj[level][i][1],20,20);
						}
					}
				}
			}
		destroy_bitmap(zed);		
	}
}


/// FUNKCE JEVCILI
/// @param o1x Levy horni roh dynamickeho objektu (bludistaka)
/// @param o1y Levy spodni roh dynamickeho objektu  (bludistaka) 
/// @param o2x Levy horni roh statickeho objektu (cile)
/// @param o2y Levy spodni roh statickeho objektu (cile)
/// @return Vraci 1 je-li v cili, v opacnem pripade vraci 0
/// Funkce pro detekci zda-li jiz staticky objekt dorazil do predem urceneho cile v bludisti
inline bool jeVcili(UINT o1x, UINT o1y, UINT o2x, UINT o2y)
{
	if((o1x == o2x) && (o1y == o2y))
		return 1;
	else
		return 0;

}


/// FUNKCE GENERUJCILE
/// Funkce pro generovani cile v zavislosti na urovni hry (cisla levelu) ze souradnic v poli souradniceCil[][][]
inline void generujCile()
{
	BITMAP *cil;
	cil = load_bitmap("./imgs/cil.bmp", 0);


	// Nastaveni cile vzdy na stejne miste prozatim v kazde urovni
	if(level >= 0)
		blit(cil, buffer, 0,0,SIRKA_OKNA-20,VYSKA_OKNA-60,20,20);

	//
	// detekce je-li pac man v cili
	if(jeVcili(o1x,o1y,SIRKA_OKNA-20,VYSKA_OKNA-60))
	{
		rectfill(buffer, 0, 20, SIRKA_OKNA, VYSKA_OKNA-40, makecol(0,0,0)); // hraci okno na cerno
		// Koncime pocitani casu a vysedek zapisujem do souboru
		tend = time(0);
		double cas = difftime(tend, tstart);
		zapisCas((int)cas);
		//
		textprintf_ex(buffer, font, SIRKA_OKNA/6, VYSKA_OKNA/2, makecol(255, 100, 200), -1, "Tuto uroven jste zvladl za: %i", (int)cas); // zobrazeni za jak dlouho byl projet level		
		if(DEBUG)
			cout << "Projeti levelu " << level+1 << " zabralo " << difftime(tend, tstart) << " sekund." << endl;        

        if(key[KEY_ENTER])
		{
         	rectfill(buffer, 0, 20, SIRKA_OKNA, VYSKA_OKNA-40, makecol(0,0,0)); // hraci okno na cerno
			level+=1;
			pl=0; // reset pacmana na vychozi/startovaci pozici
			nd=0;
			simulate_keypress(KEY_RIGHT);
			simulate_keypress(KEY_RIGHT);
			// CAS reset startovaciho casu
           tstart = time(0);
        }
	}
	delete cil; // smaz bitmapy
}


/// FUNKCE INITBLUDISTE
/// Funkce pro prvotni a nasledne inicializace celeho bludiste, vybarveni hraci plochy, nacteni mapy a cilu
inline void initBludiste()
{
	// prvni a take dalsi inicializace bludiste dat do funkce inicializace
	rectfill(buffer, 0, 0, SIRKA_OKNA, VYSKA_OKNA, makecol(47,79,79)); // vykreslim pozadi
	rectfill(buffer, 0, 20, SIRKA_OKNA, VYSKA_OKNA-40, makecol(0,0,0)); // vykreslim hraci okno na cerno
	textprintf_ex(buffer, font, SIRKA_OKNA/2, 10, makecol(255, 100, 200), -1, "UROVEN: %d", level+1); // zobrazeni levelu
	generujMapu();
	generujCile();
}


/// FUNKCE INITBUFFER
/// Funkce pro dopredne vykresleni grafiky (nasledujiciho bufferu), a zamezeni problikavani
/// Tuto funkci jsem objevil na strankach Karla Mozdrena (http://homel.vsb.cz/~moz017/cpp/?lek=13) 
void initBuffer()
{
	buffer = create_bitmap(SIRKA_OKNA,VYSKA_OKNA);
}


/// FUNKCE VYKRESLIBUFFER
/// Funkce pro dopredne vykresleni grafiky (nasledujiciho bufferu), a zamezeni problikavani
/// Tuto funkci jsem objevil na strankach Karla Mozdrena (http://homel.vsb.cz/~moz017/cpp/?lek=13) 
inline void vykresliBuffer()
{
	masked_blit(buffer, screen,0,0,0,0,SIRKA_OKNA,VYSKA_OKNA);
}


/// FUNKCE TICKER
/// Volani funkce casovace, ktere jsou inicializova v knihovne allegro a v tomto programu ve funkci inicializece()
void ticker()
{
      ticks++;
}

/// FUNKCE ZAPISCAS
/// @param t Ciselny parametr, celkovy cas za ktery hrac zvladl dany level.
/// Tato funkce ma na starost zapsani ciselne hodnoty do soubor, jmenujici se podle aktualniho cisla urovne teda napr. je-li prvni uroven tak
/// 1.txt, je-li druha uroven tak 2.txt atd.
inline void zapisCas(int t)
{
	// nejdriv prevedu int na char
	char soubor[10];
	itoa(level+1, soubor, 10); // cislo levelu
	strcat (soubor, ".txt"); // koncovka .txt
    
	// a pak zapisuju
	ofstream vystup(soubor,fstream::out | fstream::app);
	if (vystup.is_open())
	{
		vystup << t << "\n"; // zapisu cas a skocim na dalsi radek
		vystup.close();
	}
	else 
		cout << "Nemuzu otevrit soubor";    
}

/// FUNKCE ZJISTINEJCAS
/// Tato funkce cte data ze soubor pojmenovanem podle aktulani urovne, tedy je-li uroven 1 tak se soubor jmenuje 1.txt . Tyto data nacte
/// do dynamicky vytvoreneho pole, jehoz velikost se zjisti spocitanim radku v souboru. Nasleden se vyhodnoti nejnizsi prvek v poli,
/// tedy nejnizsi cas dane urovne a vraci se.
/// @return Vraci hodnotu nejnizsiho prvku nacteneho ze souboru, v opacnem pripade (neexistuje jeste soubor anebo nebyl vytvoren rekord) tak vraci 0.
int zjistiNejCas()
{
    string radek;
    int radku = 0;
 	int pomocna =0;
     
	// jmeno soubor se zklada z cisla levelu a koncovky .txt
    char soubor[3];
    itoa(level+1, soubor, 10); // cislo levelu
    strcat(soubor, ".txt"); // prida koncovku .txt

	// spocitam kolik je radku v souboru    
    ifstream vstup;
	vstup.open(soubor, fstream::in);
	//	
	while(getline(vstup,radek))
	{
		++radku;
	}
	vstup.close();
	//
	// a pak nactu data do dynamickeho pole	
	ifstream vstupik;
	vstupik.open(soubor, fstream::in);	
	
    if(radku>0)
    {
		int *pole = new int [radku];
	
		if (vstupik.is_open())
		{	
			// naplnim
			for(int i=0;i<radku;) 
			{	
				vstupik >> pole[i];
				i++; 
			}
			
			// zjistim nejmensi prvek
			pomocna=pole[0];
			for(int i=0;i<radku;i++)
			{
				if(pomocna > pole[i])
					pomocna = pole[i];
			}

			vstupik.close();
		}
		else 
		{
			cout << "Nemuzu otevrit soubor" << endl; 
		}
		// co jsem vytvoril to taky uvolnim
		delete [] pole;
     }
     else 
     {
		if(DEBUG) 
			cout << "Soubor jeste nebyl vytvoren" << endl; 
		return 0;
     }
     //
     return pomocna;
}



/// FUNKCE ITOA
/// Toto je funkce, ktera neni standartne v linuxoce sbirce ceckovskych knihoven a proto byla implementovana dodatecne !
/// Funkce je psana pod licenci GPLv3 takze ji mohu pouzit :)
char* itoa(int value, char* result, int base) 
{
	/**
	 * C++ version 0.4 char* style "itoa":
	 * Written by Luk�s Chmela
	 * Released under GPLv3.
	 */

		// check that the base if valid
		if (base < 2 || base > 36) { *result = '\0'; return result; }
	
		char* ptr = result, *ptr1 = result, tmp_char;
		int tmp_value;
	
		do {
			tmp_value = value;
			value /= base;
			*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
		} while ( value );
	
		// Apply negative sign
		if (tmp_value < 0) *ptr++ = '-';
		*ptr-- = '\0';
		while(ptr1 < ptr) {
			tmp_char = *ptr;
			*ptr--= *ptr1;
			*ptr1++ = tmp_char;
		}
		return result;
	}

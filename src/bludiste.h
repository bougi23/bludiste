/*
 *      bludiste.h
 *
 *      Copyright 01.11.2011 00:41:09 CEST Jan Dluhos  DLU0023 <jan.dluhos.st@vsb.cz> || <bougi23@seznam.cz>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */

// POPIS: Hlavickovy soubor se funkcemi, datovymi typy a globalnima promennyma

//////////////////////////
// definice datovych typu
typedef unsigned int UINT;
typedef signed int SINT;

//////////////////////////
// velikost hlavniha okna
static int VYSKA_OKNA=600; // 600
static int SIRKA_OKNA=800; //800

//////////////////////////
/// Zapnuti nebo vypnuti DEBUG modu (zapnuty vykresluje zakladni zdi a kolize na terminal)
/// Tento DEBUG mod lze spusti prikazet -d z konzotel, tedy ./bludiste -d .
UINT DEBUG = 0;

//////////////////////////
// globalni promenne
SINT pl = 0;
SINT nd = 0;
const SINT krok = 6; // default 6 rozsah je 1-6
volatile int ticks=0;
UINT level = 0;
BITMAP *buffer = new BITMAP;
bool konec = false;
// bitmapy pacmana, zatim nulove ukazatele
BITMAP *bl_h = NULL;
BITMAP *bl_d = NULL;
BITMAP *bl_l = NULL;
BITMAP *bl_p = NULL;
// promenne pro mereni casu
time_t tstart, tend; // promenne pro cas

//////////////////////////
// souradnice objektu
SINT o1x = 0;
SINT o1y = 0;
SINT o2x = 0;
SINT o2y = 0;

//////////////////////////
// prototypy fci
void inicializace(int argc, char *argv[]);
inline void ovladani();
inline bool kolize(UINT,UINT,UINT,UINT,UINT,UINT,UINT,UINT);
inline void generujMapu();
inline void generujCile();
inline bool jeVcili(UINT,UINT,UINT,UINT);
inline void initBludiste();
void zapisCas(int);
int zjistiNejCas();
//
char* itoa(int value, char* result, int base) ; // prototyp pro funkci, ktera neni v linuxu
//

// prototypy funkci na bufferovani grafiky a casovani
void initBuffer();
inline void vykresliBuffer();
void ticker();
//
